#!/bin/bash -x
rancher_server_ip=${1:-172.22.101.101}
default_password=${2:-password}
clustername=${3:-clustername}

agent_ip=`ip addr show eth1 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1`

curlprefix="appropriate"
protocol="http"

ros engine switch docker-17.03.2-ce
ros config set rancher.docker.storage_driver overlay
system-docker restart docker
sleep 5

# Login
LOGINRESPONSE=$(docker run \
    --rm \
    $curlprefix/curl \
    -s "https://$rancher_server_ip/v3-public/localProviders/local?action=login" -H 'content-type: application/json' --data-binary '{"username":"admin","password":"'$default_password'"}' --insecure)
LOGINTOKEN=$(echo $LOGINRESPONSE | jq -r .token)

# Test if cluster is created
while true; do
  ENV_STATE=$(docker run \
    --rm \
    $curlprefix/curl \
      -sLk \
      -H "Authorization: Bearer $LOGINTOKEN" \
      "https://$rancher_server_ip/v3/clusterregistrationtoken?name=$clustername" | jq -r '.data[].nodeCommand')

  if [[ "$ENV_STATE" != "null" ]]; then
    break
  else
    sleep 5
  fi
done

CLUSTERRESPONSE=$(docker run --net host \
    --rm \
    $curlprefix/curl -s "https://$rancher_server_ip/v3/clusters?name=$clustername" -H 'content-type: application/json' -H "Authorization: Bearer $LOGINTOKEN" --insecure)

# Extract clusterid to use for generating the docker run command
CLUSTERID=`echo $CLUSTERRESPONSE | jq -r .data[].id`

if [ `hostname` == "node-01" ]; then
  ROLEFLAGS="--etcd --controlplane --worker"
else
  #ROLEFLAGS="--worker"
  ROLEFLAGS="--worker"
fi

# Get token
AGENTCMD=$(docker run --net host \
    --rm \
    $curlprefix/curl -s "https://$rancher_server_ip/v3/clusterregistrationtoken?id=$CLUSTERID" -H 'content-type: application/json' -H "Authorization: Bearer $LOGINTOKEN" --insecure | jq -r .data[].nodeCommand  | head -1)

# Show the command
COMPLETECMD="$AGENTCMD $ROLEFLAGS --internal-address $agent_ip --address $agent_ip "
$COMPLETECMD

#sleep 300
#add Jenkins Project
#JENKINSPROJCMD=$(docker run --net host \
#    --rm \
#    $curlprefix/curl -s "https://$rancher_server_ip/v3/projects" -H 'content-type: application/json' -H "Authorization: Bearer $LOGINTOKEN" \
#     --data-binary '{"clusterId":"'$CLUSTERID'", "description":"Jenkins installation for CI/CD", "name":"Jenkins", "namespaceId":"namespace", "resourceQuota":null}' --insecure)

#JENKINSPROJID=$(echo $JENKINSPROJCMD | jq -r .projectId)

#JENKINSNAMESPACECMD=$(docker run --net host \
#    --rm \
#    $curlprefix/curl -s "https://$rancher_server_ip/v3/cluster/$CLUSTERID/namespaces" -H 'content-type: application/json' -H "Authorization: Bearer $LOGINTOKEN" \
#    --data-binary '{"name":"jenkinsmaster", "projectId":"'$JENKINSPROJID'", "resourceQuotaTemplateId":""}' --insecure)

#if [ `hostname` == "node-01" ]; then
#  $JENKINSNAMESPACECMD
#else
#  echo 'Jenkins step not applicable'
#fi