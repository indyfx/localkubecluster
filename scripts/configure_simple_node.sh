#!/bin/bash -x
rancher_server_ip=${1:-172.22.101.101}
default_password=${2:-password}
clustername=${3:-clustername}

agent_ip=`ip addr show eth1 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1`

mkdir -p  /home/rancher/.kube/

ros engine switch docker-1.12.6
ros config set rancher.docker.storage_driver overlay
system-docker restart docker
sleep 5

curlprefix="appropriate"
curl_prefix="appropriate"
protocol="http"

sudo add-apt-repository universe
sudo apt-get update
sudo apt-get -y install jq

# Login
LOGINRESPONSE=$(docker run --net=host \
    --rm \
    $curlprefix/curl \
    -s "https://$rancher_server_ip/v3-public/localProviders/local?action=login" -H 'content-type: application/json' --data-binary '{"username":"admin","password":"admin"}' --insecure)

LOGINTOKEN=$(echo $LOGINRESPONSE | jq -r .token)

# Test if cluster is created
while true; do
  ENV_STATE=$(docker run \
    --rm \
    $curlprefix/curl \
      -sLk \
      -H "Authorization: Bearer $LOGINTOKEN" \
      "https://$rancher_server_ip/v3/clusterregistrationtoken?name=$clustername" | jq -r '.data[].nodeCommand')

  if [[ "$ENV_STATE" != "null" ]]; then
    break
  else
    sleep 5
  fi
done

CLUSTERRESPONSE=$(docker run --net host \
    --rm \
    $curlprefix/curl -s "https://$rancher_server_ip/v3/clusters?name=$clustername" -H 'content-type: application/json' -H "Authorization: Bearer $LOGINTOKEN" --insecure)

# Extract clusterid to use for generating the docker run command
CLUSTERID=$(echo $CLUSTERRESPONSE | jq -r .data[].id)

# Get token
docker run --net host \
     --rm \
     $curlprefix/curl -s -u $LOGINTOKEN "https://$rancher_server_ip/v3/clusters/$CLUSTERID?action=generateKubeconfig" -X POST -H 'content-type: application/json' --insecure | jq -r .config > /home/rancher/.kube/config

cat /home/rancher/.kube/config

kubeVersion='1.11.2'

curl -LO curl -LO https://storage.googleapis.com/kubernetes-release/release/v$kubeVersion/bin/linux/amd64/kubectl

chmod +x ./kubectl

sudo mv ./kubectl /usr/local/bin/kubectl

curl https://raw.githubusercontent.com/helm/helm/master/scripts/get > get_helm.sh

chmod 700 get_helm.sh

./get_helm.sh