# -*- mode: ruby -*-
# vi: set ft=ruby :
require_relative 'vagrant_rancheros_guest_plugin.rb'
require_relative 'vagrant-provision-reboot-plugin.rb'

require 'ipaddr'
require 'yaml'

x = YAML.load_file('config.yaml')
puts "Config: #{x.inspect}\n\n"

$private_nic_type = x.fetch('net').fetch('private_nic_type')

Vagrant.configure(2) do |config|

    config.vm.define "server-01" do |server|
     c = x.fetch('server')
      server.vm.box= "chrisurwin/RancherOS"
      server.vm.guest = :linux
      server.vm.provider :virtualbox do |v|
        v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        v.cpus = c.fetch('cpus')
        v.linked_clone = true if Gem::Version.new(Vagrant::VERSION) >= Gem::Version.new('1.8.0') and x.fetch('linked_clones')
        v.memory = c.fetch('memory')
        v.name = "server-01"
      end
      server.vm.network x.fetch('net').fetch('network_type'), ip: x.fetch('ip').fetch('server') , nic_type: $private_nic_type
      server.vm.hostname = "server-01"
      server.vm.provision "shell", path: "scripts/configure_rancher_server.sh", args: [x.fetch('default_password'), x.fetch('version'), x.fetch('clustername')]
  end

  node_ip = IPAddr.new(x.fetch('ip').fetch('node'))
  last_node_ip = node_ip
  (1..x.fetch('node').fetch('count')).each do |i|
    c = x.fetch('node')
    hostname = "node-%02d" % i
    config.vm.define hostname do |node|
      node.vm.box   = "chrisurwin/RancherOS"
      #node.vm.box_version = x.fetch('ROS_version')
      node.vm.guest = :linux
      node.vm.boot_timeout = 600
      node.vm.provider "virtualbox" do |v|
        v.cpus = c.fetch('cpus')
        v.linked_clone = true if Gem::Version.new(Vagrant::VERSION) >= Gem::Version.new('1.8.0') and x.fetch('linked_clones')
        v.memory = c.fetch('memory')
        v.name = hostname
      end
      temp_ip = IPAddr.new(node_ip.to_i + i - 1, Socket::AF_INET).to_s
      node.vm.network x.fetch('net').fetch('network_type'), ip: temp_ip, nic_type: $private_nic_type
      node.vm.hostname = hostname
      node.vm.provision "shell", path: "scripts/configure_rancher_node.sh", args: [x.fetch('ip').fetch('server'), x.fetch('default_password'), x.fetch('clustername')]
      last_node_ip = temp_ip
    end
  end

  (1..x.fetch('admin').fetch('count')).each do |i|
    c = x.fetch('admin')
    hostname = "admin-%02d" % i
    config.vm.define hostname do |admin|
      admin.vm.box   = "chrisurwin/RancherOS"
      #admin.vm.box_version = x.fetch('ROS_version')
      admin.vm.guest = :linux
      admin.vm.boot_timeout = 600
      admin.vm.provider "virtualbox" do |v|
        v.cpus = c.fetch('cpus')
        v.linked_clone = true if Gem::Version.new(Vagrant::VERSION) >= Gem::Version.new('1.8.0') and x.fetch('linked_clones')
        v.memory = c.fetch('memory')
        v.name = hostname
      end
      adm_ip = IPAddr.new(last_node_ip.to_i + i - 1, Socket::AF_INET).to_s
      admin.vm.network x.fetch('net').fetch('network_type'), ip: adm_ip, nic_type: $private_nic_type
      admin.vm.hostname = hostname
      admin.vm.provision "shell", inline: "sudo ros console enable ubuntu"
      admin.vm.provision :unix_reboot
      admin.vm.provision "shell", path: "scripts/configure_simple_node.sh", args: [x.fetch('ip').fetch('server'), x.fetch('default_password'), x.fetch('clustername')]
      admin.vm.provision "shell", inline: "helm init"
    end
  end
end